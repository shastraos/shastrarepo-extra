### Some extra packages you can have on ShastraOS

##### To add other packages
- Clone the repo
- Open repo in your Linux Terminal (Arch recommended)
- Run this command to add packages to database

```sh
repo-add ./shastrarepo-extra.db.tar.gz package-name.pkg.tar.zst
```

